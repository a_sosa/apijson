APIJson::Application.routes.draw do
root 'guitars#index'
match "guitars/create" => "guitars#create", :via => :post
match "guitars/index" => "guitars#index", :via => :post
match "guitars/show" => "guitars#show", :via => :post
match "guitars/update" => "guitars#update", :via => :post
match "guitars/destroy" => "guitars#destroy", :via => :post

end

#Rails.application.routes.draw do
#  get 'guitars/create'

 # get 'guitars/index'

  #get 'guitars/show'

  #get 'guitars/update'

  #get 'guitars/destroy'

#end
