class GuitarsController < ApplicationController
    def create
        guitar = Guitar.new(:make => params[:make], :model => params[:model], :year => params[:year])
        if guitar.save
            error = {"error" => "0","info" => "Success"}
            render json: [error]
        else
            error = {"error" => "1","info" => "Missing params"}
            render json: [error]
        end
        
    end

    def index
        @guitar = Guitar.all

        if (params[:id]) === 1
            render json: @guitar.order(make: :asc).order(year: :asc)
        end
        if params[:id] === 0  
            render json: @guitar.order(id: :asc)
        end
    end


    def show
        @guitar = Guitar.exists?(params[:id])
        if @guitar
            render json: [Guitar.find(params[:id])]
        else
            error = {"id" => "-1", "info" => "Info not exist, maybe was removed"}
            render json: [error] 
        end
    end

    def update
        @exists = Guitar.exists?(params[:id])
        if @exists
            @guitar = Guitar.find(params[:id])
            if @guitar.update_attributes(make:params[:make], model:params[:model], year:params[:year])
                error = {"error" => "0","info" => "Success"}
                render json: [error]
            else
                error = {"error" => "1", "info" => "Missing params"}
                render json: [error]
            end            
        else
            error = {"error" => "1", "info" => "Info not exist, maybe was removed"}
            render json: [error] 
        end
    end

    def destroy
        @exist = Guitar.exists?(params[:id])
        if @exist
            @guitar = Guitar.find(params[:id])
            @guitar.destroy
            error = {"error" => "0","info" => "Success"}
            render json: [error]    
        else
            error = {"error" => "1", "info" => "Info not exist, maybe was removed"}
            render json: [error] 
        end
    end

end
