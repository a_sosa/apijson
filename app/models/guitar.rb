class Guitar < ActiveRecord::Base
	validates :make, presence: true
	validates :model, presence: true
	validates :year, presence: true
	validates :model, uniqueness: true
end
